﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beg_po_kochkam : MonoBehaviour
{
    private AudioSource stupnya;
    public AudioClip[] shagi;
    int nomerShaga;


    // Start is called before the first frame update
    void Start()
    {
       stupnya = gameObject.AddComponent<AudioSource>();
    }

   void step()
   {
      nomerShaga = Random.Range(0,shagi.Length);
      stupnya.volume = Random.Range(0.8f, 1.0f);
      stupnya.pitch = Random.Range(0.9f, 1.7f);
      stupnya.PlayOneShot(shagi[nomerShaga]);
   }
    

}
