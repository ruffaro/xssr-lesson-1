﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wind_play_sound : MonoBehaviour
{
    private const float V = 0.5f;
    private AudioSource istochnik;
    public AudioClip veter;
    // Start is called before the first frame update
    void Start()
    {
        istochnik = gameObject.AddComponent<AudioSource>();
        istochnik.volume = V;
        istochnik.PlayOneShot(veter);
    }


    
}
