using UnityEngine;
using UnityEngine.Audio;

public class Doors_Animation : MonoBehaviour
{
    Animator animator;    
    public AudioSource AS;
    public AudioClip OpenAudio;
    public AudioClip CloseAudio;
    public AudioMixerGroup Doors_Anim;
    void Start()

    {       
        animator = GetComponent<Animator>();
        AS = gameObject.AddComponent<AudioSource>();
        AS.spatialBlend = 1.0f;
        AS.rolloffMode = AudioRolloffMode.Logarithmic;
        AS.minDistance = 1;
        AS.maxDistance = 12;
        AS.outputAudioMixerGroup = Doors_Anim;
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {           
            Doors("open");
            AS.PlayOneShot(OpenAudio);
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {           
            Doors("close");
            AS.PlayOneShot(CloseAudio);
        }
    }
    void Doors(string direction)
    {
           animator.SetTrigger(direction);
    }
   
    
	
}
