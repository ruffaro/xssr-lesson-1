﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wind_interior : MonoBehaviour
{
    private GameObject player;
    private AudioSource wind;
    private AudioSource roomtoneSource;
    public AudioClip roomtoneClip;
    public GameObject windGO;

    // Start is called before the first frame update
    void Start()
    {
     player = GameObject.FindGameObjectWithTag("Player");
     wind = windGO.GetComponent<AudioSource>();

     roomtoneSource = gameObject.AddComponent<AudioSource>();
     roomtoneSource.loop = true;
     roomtoneSource.clip = roomtoneClip;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            wind.Stop();
            roomtoneSource.Play();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            wind.Play();
            roomtoneSource.Stop();
        }
    }
}
